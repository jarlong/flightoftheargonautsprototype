﻿/************************************************************************************

Copyright   :   Copyright 2017-Present Oculus VR, LLC. All Rights reserved.

Licensed under the Oculus VR Rift SDK License Version 3.2 (the "License");
you may not use the Oculus VR Rift SDK except in compliance with the License,
which is provided at the time of installation or download, or which
otherwise accompanies this software in either electronic or hard copy form.

You may obtain a copy of the License at

http://www.oculusvr.com/licenses/LICENSE-3.2

Unless required by applicable law or agreed to in writing, the Oculus VR SDK
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

************************************************************************************/

using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class RawInteraction : MonoBehaviour {
    public Material oldHoverMat;
    public Material hoverMat;

    public Material oldSelectedMat;
    public Material selectedMat;

    public GameObject oldtag;

    private GameObject oldSelection;
    private bool firstSelect = true;

    public void OnHoverEnter(Transform t) {

        if (t.GetComponent<NavMeshAgent>() == null)
        {
            return;
        }
        else if(t.gameObject.tag == "selectedShip")
        {
            return;
        }
        else
        {
            Debug.Log("hello i work");
            oldHoverMat = t.gameObject.GetComponent<MeshRenderer>().material;
            t.gameObject.GetComponent<MeshRenderer>().material = hoverMat;

        }
    }

    public void OnHoverExit(Transform t) {

        if (t.GetComponent<NavMeshAgent>() == null)
        {
            return;
        }
        else if (t.gameObject.tag == "selectedShip")
        {
            return;
        }
        else {
            t.gameObject.GetComponent<MeshRenderer>().material = oldHoverMat;
        }
    }

    public void OnSelected(Transform t) {


        if(t.GetComponent<NavMeshAgent>() == null)
        {
            return;
        }
        else
        {
            if (firstSelect)
            {
                oldSelectedMat = t.gameObject.GetComponent<MeshRenderer>().material;
                t.gameObject.GetComponent<MeshRenderer>().material = selectedMat;
                oldtag.tag = t.tag;
                t.gameObject.tag = "selectedShip";
                firstSelect = false;
            }
            else
            {
                oldSelection = GameObject.FindGameObjectWithTag("selectedShip");

                oldSelection.tag = oldtag.tag;
                oldSelection.GetComponent<MeshRenderer>().material = oldSelectedMat;


                oldtag.tag = t.tag;
                oldSelectedMat = t.gameObject.GetComponent<MeshRenderer>().material;

                t.gameObject.tag = "selectedShip";
                t.gameObject.GetComponent<MeshRenderer>().material = selectedMat;
            }
        }
        
        
        //if (t.gameObject.name == "BackButton") {
          //  SceneManager.LoadScene("main", LoadSceneMode.Single);
       // }
       // Debug.Log("Clicked on " + t.gameObject.name);
        //if (outText != null) {
        //    outText.text = "<b>Last Interaction:</b>\nClicked On:" + t.gameObject.name;
        //}
    }
}
