﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LargeShipProperties : MonoBehaviour {

    public float flightSpeed;
    public float turnRate;
    public int hitPoints;
    public float attackRange;
    public float attackRate;
    public float attackPower;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
